package com.lardi.core.controller;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.User;
import com.lardi.core.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactRestController.class)
public class ContactRestControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserService userService;

  private User user;
  private Contact contact;

  @Before
  public void setUp() throws Exception {
    user = User.builder()
            .build();
    contact = Contact.builder()
            .user(user)
            .mobilePhone("+123")
            .build();
  }

  @Test
  public void should_redirect_unauth_users() throws Exception {
    mockMvc.perform(get("/contacts")).andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrlPattern("**/auth/login"));
  }

  @Test
  @WithMockUser
  public void getAllContactsFromCurrentUser() throws Exception {
    when(userService.getAllContactsFromCurrentUser()).thenReturn(Collections.singleton(contact));
    mockMvc.perform(get("/contacts"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$[0].mobilePhone", is("+123")))
            .andDo(print());
  }

  @Test
  public void addOrUpdateContactForCurrentUser() {
  }

  @Test
  public void updateContactForCurrentUser() {
  }

  @Test
  public void deleteContactFromCurrentUser() {
  }
}