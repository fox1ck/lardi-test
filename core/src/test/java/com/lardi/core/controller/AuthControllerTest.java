package com.lardi.core.controller;

import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.repository.CustomUserRepository;
import com.lardi.core.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuthController.class)
@RunWith(SpringRunner.class)
public class AuthControllerTest {

  @Autowired
  MockMvc mockMvc;

  @MockBean
  UserService userService;

  @MockBean
  CustomUserRepository customUserRepository;

  private RegisterRequest testLogin;
  private User user;

  @Before
  public void setUp() throws Exception {
    testLogin = RegisterRequest.builder()
            .login("user")
            .build();
    user = User.builder()
            .login("user")
            .passwordHash("$2a$04$amDlj64COspsvywMQQfUVuDt.QbDfs0DvJbPQ9zpT.D1ByM9bP9e6")
            .build();
  }

  @Test
  public void login() throws Exception {
    when(customUserRepository.findByLogin("user")).thenReturn(Optional.of(user));
    mockMvc.perform(
            formLogin("/auth/login").user("user").password("password"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/"));
    mockMvc.perform(
            formLogin("/auth/login").user("user").password("wrongPass"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/auth/login?error"));
  }

  @Test
  public void logout() throws Exception {
    mockMvc.perform(get("/auth/logout"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/auth/login?logout"));
  }

  @Test
  public void register_correct() throws Exception {
    LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("login", "vasya");
    params.add("password", "Petya");
    params.add("info.firstName", "Saws1");
    params.add("info.lastName", "Saws1ss");
    params.add("info.middleName", "Saws1s");
    mockMvc.perform(post("/auth/register").params(params)).andDo(print());
  }

  @Test
  public void showRegisterPage() throws Exception {
    mockMvc.perform(get("/auth/register"))
            .andExpect(status().isOk())
            .andExpect(view().name("register"))
            .andExpect(content().string(containsString("Register")));
  }

  @Test
  public void showLoginPage() throws Exception {
    mockMvc.perform(get("/auth/login"))
            .andExpect(status().isOk())
            .andExpect(view().name("login"))
            .andExpect(content().string(containsString("Please login to proceed.")));
  }

}