package com.lardi.core.service;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactInfo;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.exception.LoginAlreadyUsedException;
import com.lardi.core.repository.CustomUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest(excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@TestPropertySource(properties = {"storage.type=db"})
@ActiveProfiles("jpatest, test")
public class JpaUserServiceTest {

  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private CustomUserRepository customUserRepository;

  @Autowired
  private JpaUserService jpaUserService;

  private User testUser;
  private Contact testContact;
  private RegisterRequest registerRequest;
  private BCryptPasswordEncoder passwordEncoder;

  @Before
  public void setUp() throws Exception {
    testUser = User.builder().passwordHash("someHash").login("someLogin").build();
    testContact = Contact.builder()
            .info(
                    ContactInfo.builder()
                            .firstName("someName1")
                            .lastName("someName2")
                            .middleName("someName3")
                            .build()
            ).mobilePhone("+380(66)2950289").build();
    registerRequest = new RegisterRequest();
    registerRequest.setLogin("testLogin");
    registerRequest.setPassword("somePass");
    passwordEncoder = new BCryptPasswordEncoder();
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Authentication authentication = Mockito.mock(Authentication.class);
    SecurityContextHolder.setContext(securityContext);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    Mockito.when(securityContext.getAuthentication().getName()).thenReturn("someLogin");
  }

  @Test
  public void addContactToCurrentUser() {
    testEntityManager.persist(testUser);
    testEntityManager.flush();
    Contact contact = jpaUserService.addContactToCurrentUser(testContact);
    assertThat(contact).isNotNull();
    assertThat(contact.getId()).isNotNull();
    User user = jpaUserService.findUserByLogin(testUser.getLogin()).get();
    assertThat(user.getContacts()).hasSize(1);
  }

  @Test(expected = LoginAlreadyUsedException.class)
  public void should_throw_exception_when_user_with_same_login_already_exist() {
    jpaUserService.registerUser(registerRequest);
    jpaUserService.registerUser(registerRequest);
  }

  @Test
  public void registerUser() {
    User user = jpaUserService.registerUser(registerRequest);
    assertThat(user).isNotNull();
    assertThat(user.getId()).isNotNull();
    assertThat(user.getLogin()).isEqualTo(registerRequest.getLogin());
    User user1 = testEntityManager.find(User.class, user.getId());
    assertThat(user1).isNotNull();
    assertThat(user1.getId()).isEqualTo(user1.getId());
    assertThat(passwordEncoder.matches("somePass", user.getPasswordHash())).isTrue();
  }
}