package com.lardi.core.service;

import com.lardi.core.config.StorageProperties;
import com.lardi.core.domain.Contact;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.exception.DuplicateMobilePhoneException;
import com.lardi.core.exception.LoginAlreadyUsedException;
import com.lardi.core.repository.JsonUserRepository;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonUserServiceTest {

  private JsonUserService userService;
  private RegisterRequest registerRequest;
  private StorageProperties storageProperties;
  private BCryptPasswordEncoder passwordEncoder;
  private User testUser;
  private Contact testContact;

  @Before
  public void setUp() throws Exception {
    storageProperties = new StorageProperties();
    userService = new JsonUserService(storageProperties);
    passwordEncoder = new BCryptPasswordEncoder();
    userService.setPasswordEncoder(passwordEncoder);
    userService.setCustomUserRepository(new JsonUserRepository(storageProperties));
    Files.createDirectories(Paths.get(storageProperties.getDataDir()));
    registerRequest = new RegisterRequest();
    registerRequest.setLogin("testLogin");
    registerRequest.setPassword("somePass");
    testUser = User.builder().passwordHash("someHash").login("someLogin").build();
    testContact = Contact.builder().mobilePhone("phoneNumber").build();
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Authentication authentication = Mockito.mock(Authentication.class);
    SecurityContextHolder.setContext(securityContext);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    Mockito.when(securityContext.getAuthentication().getName()).thenReturn("someLogin");
  }

  @After
  public void tearDown() throws Exception {
    FileUtils.deleteDirectory(Paths.get(storageProperties.getDataDir()).toFile());
  }

  @Test
  public void registerUser() {
    User user = userService.registerUser(registerRequest);
    assertThat(user).isNotNull();
    assertThat(user.getId()).isNotNull();
    assertThat(user.getLogin()).isEqualTo(registerRequest.getLogin());
    assertThat(passwordEncoder.matches("somePass", user.getPasswordHash())).isTrue();
  }

  @Test(expected = LoginAlreadyUsedException.class)
  public void should_throw_exception_when_user_with_same_login_alraady_exist() {
    userService.registerUser(registerRequest);
    userService.registerUser(registerRequest);
  }

  @Test
  public void saveUser() {
    User user = userService.saveUser(testUser);
    assertThat(user).isNotNull();
    assertThat(user.getId()).isNotNull();
    assertThat(user.getPasswordHash()).isEqualTo("someHash");
    assertThat(user.getLogin()).isEqualTo("someLogin");
  }

  @Test(expected = DuplicateMobilePhoneException.class)
  public void should_throw_exception_when_adding_contact_with_same_mobile_phone() {
    userService.saveUser(testUser);
    userService.addContactToCurrentUser(testContact);
    userService.addContactToCurrentUser(testContact);
  }

  @Test
  public void addContactToCurrentUser() {
    userService.saveUser(testUser);
    Contact contact = userService.addContactToCurrentUser(testContact);
    assertThat(contact).isNotNull();
    assertThat(contact.getId()).isNotNull();
    User userByLogin = userService.findUserByLogin(testUser.getLogin()).get();
    boolean b = userByLogin.getContacts().stream().anyMatch(c -> c.getId().equals(contact.getId()));
    assertThat(b).isTrue();
    assertThat(userByLogin.getContacts()).hasSize(1);
  }

  @Test
  public void deleteContactFromCurrentUser() {
    userService.saveUser(testUser);
    Contact contact = userService.addContactToCurrentUser(testContact);
    userService.deleteContactFromCurrentUser(contact.getId());
    User userByLogin = userService.findUserByLogin(testUser.getLogin()).get();
    assertThat(userByLogin.getContacts()).hasSize(0);
  }
}