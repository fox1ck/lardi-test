package com.lardi.core.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"storage.type = json"})
public class JsonStorageContextTest {

  @Autowired
  ApplicationContext context;

  @Test
  public void must_be_not_null() {
    assertThat(context.getBean(JsonStorageConfig.class)).isNotNull();
  }

  @Test(expected = NoSuchBeanDefinitionException.class)
  public void must_be_null() {
    context.getBean(JpaStorageConfig.class);
  }
}