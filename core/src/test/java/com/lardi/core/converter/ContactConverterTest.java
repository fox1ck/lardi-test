package com.lardi.core.converter;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactDto;
import com.lardi.core.domain.ContactInfo;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ContactConverterTest {

  private Contact contact;

  private ContactConverter contactConverter = new ContactConverter();

  @Before
  public void setUp() {
    contact = Contact.builder()
            .mobilePhone("test1")
            .info(ContactInfo.builder().build())
            .email("test2")
            .address("test3")
            .homePhone("test4")
            .build();
    contact.setId(12345L);
  }

  @Test
  public void convert() {
    ContactDto dto = contactConverter.convert(contact);
    assertThat(dto.getId()).isEqualTo(contact.getId());
    assertThat(dto.getInfo()).isEqualTo(contact.getInfo());
    assertThat(dto.getMobilePhone()).isEqualTo(contact.getMobilePhone());
    assertThat(dto.getHomePhone()).isEqualTo(contact.getHomePhone());
    assertThat(dto.getEmail()).isEqualTo(contact.getEmail());
    assertThat(dto.getAddress()).isEqualTo(contact.getAddress());
  }

}