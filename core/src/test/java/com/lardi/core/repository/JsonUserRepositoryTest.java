package com.lardi.core.repository;

import com.lardi.core.config.StorageProperties;
import com.lardi.core.domain.User;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonUserRepositoryTest {

  private JsonUserRepository repo;
  private StorageProperties storageProperties;
  private User testUser;

  @Before
  public void setUp() throws IOException {
    storageProperties = new StorageProperties();
    storageProperties.setDataDir(Paths.get(System.getProperty("java.io.tmpdir"), "test_json_storage").toString());
    Files.createDirectories(Paths.get(storageProperties.getDataDir()));
    repo = new JsonUserRepository(storageProperties);
    testUser = new User();
    testUser.setLogin("testLoginUser");
    testUser = repo.save(testUser);
  }

  @After
  public void tearDown() throws Exception {
    FileUtils.deleteDirectory(Paths.get(storageProperties.getDataDir()).toFile());
  }

  @Test
  public void whenFindByLogin_thenReturnUser() {
    User foundUser = repo.findByLogin(testUser.getLogin()).get();
    assertThat(foundUser.getLogin())
            .isEqualTo(testUser.getLogin());
  }

  @Test
  public void whenSave_thenIdMustBeNotNull() {
    assertThat(testUser.getId()).isNotNull();
  }
}