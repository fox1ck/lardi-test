package com.lardi.core.exception;

public class DuplicateMobilePhoneException extends RuntimeException{

  public DuplicateMobilePhoneException() {
    super();
  }

  public DuplicateMobilePhoneException(String message) {
    super(message);
  }

  public DuplicateMobilePhoneException(String message, Throwable cause) {
    super(message, cause);
  }

  public DuplicateMobilePhoneException(Throwable cause) {
    super(cause);
  }

  protected DuplicateMobilePhoneException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
