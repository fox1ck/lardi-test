package com.lardi.core.exception;

public class LoginAlreadyUsedException extends RuntimeException {

  public LoginAlreadyUsedException() {
    super();
  }

  public LoginAlreadyUsedException(String message) {
    super(message);
  }

  public LoginAlreadyUsedException(String message, Throwable cause) {
    super(message, cause);
  }

  public LoginAlreadyUsedException(Throwable cause) {
    super(cause);
  }

  protected LoginAlreadyUsedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
