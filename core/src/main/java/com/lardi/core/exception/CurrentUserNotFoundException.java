package com.lardi.core.exception;

public class CurrentUserNotFoundException extends RuntimeException{

  public CurrentUserNotFoundException() {
    super();
  }

  public CurrentUserNotFoundException(String message) {
    super(message);
  }

  public CurrentUserNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public CurrentUserNotFoundException(Throwable cause) {
    super(cause);
  }

  protected CurrentUserNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
