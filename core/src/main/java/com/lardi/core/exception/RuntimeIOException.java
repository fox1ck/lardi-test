package com.lardi.core.exception;

public class RuntimeIOException extends RuntimeException{

  public RuntimeIOException() {
    super();
  }

  public RuntimeIOException(String message) {
    super(message);
  }

  public RuntimeIOException(String message, Throwable cause) {
    super(message, cause);
  }

  public RuntimeIOException(Throwable cause) {
    super(cause);
  }

  protected RuntimeIOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
