package com.lardi.core.config;

import com.lardi.core.helper.InitialUserRegistrationHelper;
import com.lardi.core.helper.JpaInitializer;
import com.lardi.core.repository.CustomUserRepository;
import com.lardi.core.service.JpaUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableAutoConfiguration
@ConditionalOnProperty(name = "storage.type", havingValue = "db")
@EnableJpaRepositories(basePackages = {"com.lardi.core.repository"})
@EntityScan(basePackages = {"com.lardi.core.domain"})
@RequiredArgsConstructor
public class JpaStorageConfig {

  private final PasswordEncoder passwordEncoder;
  private final CustomUserRepository customUserRepository;
  private final JdbcTemplate jdbcTemplate;

  @Bean
  JpaUserService jpaUserService() {
    JpaUserService jpaUserService = new JpaUserService();
    jpaUserService.setPasswordEncoder(passwordEncoder);
    jpaUserService.setCustomUserRepository(customUserRepository);
    return jpaUserService;
  }

  @Bean
  @Profile("!test")
  InitialUserRegistrationHelper initialUserRegistrationHelper() {
    return new InitialUserRegistrationHelper(jpaUserService());
  }

  @Bean
  @Profile("!test")
  JpaInitializer jpaInitializer() {
    return new JpaInitializer(jdbcTemplate, initialUserRegistrationHelper());
  }

}
