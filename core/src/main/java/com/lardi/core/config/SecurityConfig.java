package com.lardi.core.config;

import com.lardi.core.repository.CustomUserRepository;
import com.lardi.core.service.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
@Profile("!jpatest")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final CustomUserRepository userRepo;
  private final PasswordEncoder passwordEncoder;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/auth/**").permitAll()
            .antMatchers("/css/**", "/js/**", "/webjars/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/auth/login")
            .permitAll()
            .and()
            .logout()
            .logoutUrl("/auth/logout")
            .logoutSuccessUrl("/auth/login?logout")
            .permitAll();
    http.csrf().disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authProvider());
    auth.userDetailsService(customUserDetailsService());
  }

  @Bean
  DaoAuthenticationProvider authProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService());
    authProvider.setPasswordEncoder(passwordEncoder);
    return authProvider;
  }

  @Bean
  UserDetailsService customUserDetailsService() {
    return new CustomUserDetailsService(userRepo);
  }

}
