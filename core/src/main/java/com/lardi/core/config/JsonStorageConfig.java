package com.lardi.core.config;

import com.lardi.core.helper.InitialUserRegistrationHelper;
import com.lardi.core.helper.JsonInitializer;
import com.lardi.core.repository.JsonUserRepository;
import com.lardi.core.service.JsonUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@ConditionalOnProperty(name = "storage.type", havingValue = "json", matchIfMissing = true)
@RequiredArgsConstructor
public class JsonStorageConfig {

  private final StorageProperties properties;
  private final PasswordEncoder passwordEncoder;

  @Bean
  JsonUserRepository jsonRepository() {
    return new JsonUserRepository(properties);
  }

  @Bean
  JsonUserService jsonUserService() {
    JsonUserService jus = new JsonUserService(properties);
    jus.setCustomUserRepository(jsonRepository());
    jus.setPasswordEncoder(passwordEncoder);
    return jus;
  }

  @Bean
  @Profile("!test")
  InitialUserRegistrationHelper initialUserRegistrationHelper() {
    return new InitialUserRegistrationHelper(jsonUserService());
  }

  @Bean
  @Profile("!test")
  JsonInitializer jsonInitializer(StorageProperties properties) {
    return new JsonInitializer(properties, initialUserRegistrationHelper());
  }

}
