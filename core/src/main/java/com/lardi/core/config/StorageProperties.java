package com.lardi.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Paths;

@ConfigurationProperties("storage")
@Data
@Component
public class StorageProperties {
  private StorageType type = StorageType.JSON;
  private String dataDir = Paths.get(System.getProperty("java.io.tmpdir"), "json_storage").toString();

  enum StorageType {
    JSON, DB
  }
}

