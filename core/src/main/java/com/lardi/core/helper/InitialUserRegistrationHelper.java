package com.lardi.core.helper;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactInfo;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.service.UserService;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class InitialUserRegistrationHelper {

  private final UserService userService;

  public void createUsers() {
    RegisterRequest request = new RegisterRequest();
    request.setLogin("admin");
    request.setPassword("admin");
    request.setInfo(
            ContactInfo.builder()
                    .firstName("Petrov")
                    .lastName("Pavel")
                    .middleName("Ivanovich")
                    .build()
    );
    User user = userService.registerUser(request);
    Contact contact1 = Contact.builder()
            .email("first@example.com")
            .mobilePhone("+380(66)2950289")
            .info(
                    ContactInfo.builder()
                            .firstName("Name 1")
                            .lastName("Name 2")
                            .middleName("Name 3")
                            .build()
            ).build();
    Contact contact2 = Contact.builder()
            .email("second@example.com")
            .mobilePhone("+380(65)1234578")
            .info(
                    ContactInfo.builder()
                            .firstName("Name 11")
                            .lastName("Name 22")
                            .middleName("Name 32")
                            .build()
            ).build();
    contact1.setUser(user);
    contact2.setUser(user);
    user.setContacts(Stream.of(contact1, contact2).collect(Collectors.toSet()));
    userService.saveUser(user);
    request.setLogin("user");
    request.setPassword("user");
    userService.registerUser(request);
  }
}
