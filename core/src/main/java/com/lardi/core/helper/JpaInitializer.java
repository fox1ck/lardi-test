package com.lardi.core.helper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.FileUrlResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.net.URL;

public class JpaInitializer implements CommandLineRunner {

  private final JdbcTemplate template;
  private final InitialUserRegistrationHelper initialUserRegistrationHelper;

  public JpaInitializer(JdbcTemplate template,
                        InitialUserRegistrationHelper initialUserRegistrationHelper) {
    this.template = template;
    this.initialUserRegistrationHelper = initialUserRegistrationHelper;
  }

  @Override
  public void run(String... args) throws Exception {
    URL resource = Thread.currentThread().getContextClassLoader().getResource("schema_to_execute.sql");
    ScriptUtils.executeSqlScript(template.getDataSource().getConnection(), new FileUrlResource(resource));
    initialUserRegistrationHelper.createUsers();
  }


}
