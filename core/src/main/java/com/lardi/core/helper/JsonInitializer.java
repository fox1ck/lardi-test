package com.lardi.core.helper;

import com.lardi.core.config.StorageProperties;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.boot.CommandLineRunner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonInitializer implements CommandLineRunner {

  private final StorageProperties storageProperties;
  private final InitialUserRegistrationHelper initialUserRegistrationHelper;

  public JsonInitializer(StorageProperties storageProperties, InitialUserRegistrationHelper initialUserRegistrationHelper) {
    this.storageProperties = storageProperties;
    this.initialUserRegistrationHelper = initialUserRegistrationHelper;
  }

  @Override
  public void run(String... args) throws Exception {
    Path path = Paths.get(storageProperties.getDataDir());
    FileUtils.deleteDirectory(path.toFile());
    Files.createDirectories(path);
    initialUserRegistrationHelper.createUsers();
  }
}
