package com.lardi.core.service;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {

  User saveUser(User user);

  User registerUser(RegisterRequest request);

  Contact updateContactForCurrentUser(Long contactId, Contact contact);

  Contact addContactToCurrentUser(Contact contact);

  Set<Contact> getAllContactsFromCurrentUser();

  void deleteContactFromCurrentUser(Long contactId);

  Optional<User> findUserByLogin(String login);
}
