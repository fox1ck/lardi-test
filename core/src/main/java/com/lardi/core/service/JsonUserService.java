package com.lardi.core.service;

import com.lardi.core.config.StorageProperties;
import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactInfo;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.exception.LoginAlreadyUsedException;
import com.lardi.core.exception.RuntimeIOException;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class JsonUserService extends AbstractUserService {

  private final StorageProperties storageProperties;

  @Override
  public Contact addContactToCurrentUser(Contact contact) {
    User currentUser = loadCurrentUser();
    checkMobileDuplicate(currentUser, contact);
    if (currentUser.getContacts() == null) {
      currentUser.setContacts(new HashSet<>());
    }
    currentUser.getContacts().add(contact);
    saveUser(currentUser);
    return contact;
  }

  @Override
  public User registerUser(RegisterRequest request) {
    String dataDir = storageProperties.getDataDir();
    Path path = Paths.get(dataDir);
    try (Stream<Path> walk = Files.walk(path)) {
      Optional<Path> any = walk.filter(f -> f.getFileName().toString().equals(request.getLogin())).findAny();
      if (any.isPresent()) {
        throw new LoginAlreadyUsedException(request.getLogin());
      }
    } catch (IOException e) {
      throw new RuntimeIOException(e);
    }
    ContactInfo info = request.getInfo();
    User build = User.builder()
            .login(request.getLogin())
            .passwordHash(passwordEncoder.encode(request.getPassword()))
            .info(info == null ? null :
                    ContactInfo.builder()
                            .firstName(info.getFirstName())
                            .lastName(info.getLastName())
                            .middleName(info.getMiddleName())
                            .build()
            ).build();
    return customUserRepository.save(build);
  }

}
