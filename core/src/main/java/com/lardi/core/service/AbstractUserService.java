package com.lardi.core.service;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.User;
import com.lardi.core.exception.ContactNotFoundException;
import com.lardi.core.exception.CurrentUserNotFoundException;
import com.lardi.core.exception.DuplicateMobilePhoneException;
import com.lardi.core.repository.CustomUserRepository;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractUserService implements UserService {

  PasswordEncoder passwordEncoder;
  CustomUserRepository customUserRepository;

  @Required
  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  @Required
  public void setCustomUserRepository(CustomUserRepository customUserRepository) {
    this.customUserRepository = customUserRepository;
  }

  @Override
  public User saveUser(User user) {
    return customUserRepository.save(user);
  }

  @Override
  public Set<Contact> getAllContactsFromCurrentUser() {
    Set<Contact> contacts = loadCurrentUser().getContacts();
    return contacts == null ? Collections.emptySet() : contacts;
  }

  @Override
  public Contact updateContactForCurrentUser(Long contactId, Contact contact) {
    User currentUser = loadCurrentUser();
    Optional<Contact> any = currentUser.getContacts().stream()
            .filter(c -> c.getId().equals(contactId))
            .findAny();
    if (any.isPresent()) {
      currentUser.getContacts().remove(any.get());
    } else {
      throw new ContactNotFoundException();
    }
    checkMobileDuplicate(currentUser, contact);
    currentUser.getContacts().add(contact);
    beforeSave(currentUser, contact);
    saveUser(currentUser);
    return contact;
  }

  void beforeSave(User currentUser, Contact contact) {

  }

  @Override
  public void deleteContactFromCurrentUser(Long contactId) {
    User user = loadCurrentUser();
    user.setContacts(user.getContacts().stream().filter(c -> !c.getId().equals(contactId)).collect(Collectors.toSet()));
    saveUser(user);
  }

  @Override
  public Optional<User> findUserByLogin(String login) {
    return customUserRepository.findByLogin(login);
  }

  void checkMobileDuplicate(User currentUser, Contact contact) {
    Set<Contact> contacts = currentUser.getContacts();
    if (contacts != null && contacts.stream().anyMatch(c -> c.getMobilePhone().equals(contact.getMobilePhone()))) {
      throw new DuplicateMobilePhoneException(contact.getMobilePhone());
    }
  }

  User loadCurrentUser() {
    return findUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName())
            .orElseThrow(CurrentUserNotFoundException::new);
  }

}
