package com.lardi.core.service;

import com.lardi.core.domain.User;
import com.lardi.core.repository.CustomUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

  private final CustomUserRepository userService;

  @Override
  public UserDetails loadUserByUsername(String username) {
    User user = userService.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username + " was not found on the server"));
    return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPasswordHash(), getAuthorities(Collections.singletonList("ROLE_USER")));
  }

  private static List<GrantedAuthority> getAuthorities(List<String> roles) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    for (String role : roles) {
      authorities.add(new SimpleGrantedAuthority(role));
    }
    return authorities;
  }
}
