package com.lardi.core.service;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactInfo;
import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.domain.User;
import com.lardi.core.exception.DuplicateMobilePhoneException;
import com.lardi.core.exception.LoginAlreadyUsedException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashSet;
import java.util.Optional;

public class JpaUserService extends AbstractUserService {

  @Override
  public Contact addContactToCurrentUser(Contact contact) {
    Contact res = null;
    User user = loadCurrentUser();
    try {
      if (user.getContacts() == null) {
        user.setContacts(new HashSet<>());
      }
      user.getContacts().add(contact);
      contact.setUser(user);
      user = saveUser(user);
    } catch (DataIntegrityViolationException e) {
      throw new DuplicateMobilePhoneException(contact.getMobilePhone(), e);
    }
    Optional<Contact> tempContact = user.getContacts().stream()
            .filter(c -> c.getMobilePhone().equals(contact.getMobilePhone()))
            .findAny();
    if (tempContact.isPresent()) {
      res = tempContact.get();
    }
    return res;
  }

  @Override
  public User registerUser(RegisterRequest request) {
    User res;
    try {
      ContactInfo info = request.getInfo();
      User build = User.builder()
              .login(request.getLogin())
              .passwordHash(passwordEncoder.encode(request.getPassword()))
              .info(info == null ? null :
                      ContactInfo.builder()
                              .firstName(info.getFirstName())
                              .lastName(info.getLastName())
                              .middleName(info.getMiddleName())
                              .build()
              ).build();
      res = customUserRepository.save(build);
    } catch (DataIntegrityViolationException e) {
      throw new LoginAlreadyUsedException(request.getLogin(), e);
    }
    return res;
  }

  @Override
  void beforeSave(User currentUser, Contact contact) {
    contact.setUser(currentUser);
  }
}
