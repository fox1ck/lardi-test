package com.lardi.core.controller;

import com.lardi.core.exception.DuplicateMobilePhoneException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
public class ErrorHandlerController {

  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public String processValidationError(MethodArgumentNotValidException ex) {
    return handleError(ex.getBindingResult());
  }

  @ExceptionHandler({BindException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public String processValidationError(BindException ex) {
    return handleError(ex.getBindingResult());
  }

  @ExceptionHandler(DuplicateMobilePhoneException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public String handleDuplication(DuplicateMobilePhoneException ex) {
    return "Duplicate mobile phone: " + ex.getMessage();
  }

  private String handleError(BindingResult bindingResult) {
    List<FieldError> fieldErrors = bindingResult.getFieldErrors();
    StringBuilder sb = new StringBuilder("Errors: \r\n");
    for (FieldError fieldError : fieldErrors) {
      sb.append(
              String.format("field: %s, rejectedValue: %s, msg: %s",
                      fieldError.getField(),
                      fieldError.getRejectedValue(),
                      fieldError.getDefaultMessage()
              )).append("\r\n");
    }
    return sb.toString();
  }


}
