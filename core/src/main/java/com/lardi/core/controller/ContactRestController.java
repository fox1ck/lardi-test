package com.lardi.core.controller;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactDto;
import com.lardi.core.converter.ContactConverter;
import com.lardi.core.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

@RestController
@RequestMapping("contacts")
@RequiredArgsConstructor
public class ContactRestController {

  private final UserService userService;
  private final ContactConverter contactConverter;

  @GetMapping
  public Collection<ContactDto> getAllContactsFromCurrentUser() {
    return contactConverter.convert(userService.getAllContactsFromCurrentUser());
  }

  @PostMapping
  public ContactDto addContactToCurrentUser(@RequestBody @Valid Contact contact) {
    return contactConverter.convert(userService.addContactToCurrentUser(contact));
  }

  @PutMapping("{contactId}")
  public ContactDto updateContactForCurrentUser(@PathVariable("contactId") @Min(0) @Max(Long.MAX_VALUE) @NotEmpty Long contactId,
                                                @RequestBody @Valid Contact contact) {
    return contactConverter.convert(userService.updateContactForCurrentUser(contactId, contact));
  }

  @DeleteMapping("{contactId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteContactFromCurrentUser(@PathVariable("contactId") @Min(0) @Max(Long.MAX_VALUE) @NotEmpty Long contactId) {
    userService.deleteContactFromCurrentUser(contactId);
  }
}
