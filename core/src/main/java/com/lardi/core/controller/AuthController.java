package com.lardi.core.controller;

import com.lardi.core.domain.RegisterRequest;
import com.lardi.core.exception.LoginAlreadyUsedException;
import com.lardi.core.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("auth")
@RequiredArgsConstructor
public class AuthController {

  private final UserService userService;

  @PostMapping(value = "register")
  public String register(@ModelAttribute @Valid RegisterRequest request, Model model) {
    try {
      userService.registerUser(request);
    } catch (LoginAlreadyUsedException e) {
      model.addAttribute("error", "Login already used");
      return "register";
    }
    return "login";
  }

  @GetMapping(value = "register")
  public String showRegisterPage() {
    return "register";
  }

  @RequestMapping(value = "login")
  public String showLoginPage(Model model,
                              @RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {
    if (error != null) {
      model.addAttribute("error", "Invalid username and password!");
    }
    if (logout != null) {
      model.addAttribute("msg", "You've been logged out successfully.");
    }
    return "login";
  }

}
