package com.lardi.core.converter;

import com.lardi.core.domain.Contact;
import com.lardi.core.domain.ContactDto;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactConverter {

  public ContactDto convert(Contact entity) {
    if (entity != null) {
      return ContactDto.builder()
              .id(entity.getId())
              .info(entity.getInfo())
              .mobilePhone(entity.getMobilePhone())
              .homePhone(entity.getHomePhone())
              .email(entity.getEmail())
              .address(entity.getAddress())
              .build();
    }
    return null;
  }

  public List<ContactDto> convert(Collection<Contact> entities) {
    if (entities == null) {
      return Collections.emptyList();
    }
    return entities.stream().map(this::convert).collect(Collectors.toList());
  }
}
