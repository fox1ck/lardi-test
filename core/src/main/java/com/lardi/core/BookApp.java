package com.lardi.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootConfiguration
@ComponentScan
@PropertySource(value = "file:${lardi.conf}", ignoreResourceNotFound = true)
public class BookApp {
  public static void main(String[] args) {
    SpringApplication.run(BookApp.class, args);
  }
}
