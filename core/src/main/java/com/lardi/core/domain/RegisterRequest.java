package com.lardi.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest {

  @NotEmpty
  @Size(min = 3, max = 16)
  @Pattern(regexp = "^[A-z]+$", message = "Allowed only latin characters")
  private String login;

  @NotEmpty
  @Size(min = 5, max = 16)
  private String password;

  @Valid
  @NotNull
  private ContactInfo info;
}
