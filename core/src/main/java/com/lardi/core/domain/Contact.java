package com.lardi.core.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name = "contact")
@Entity
@Data
@ToString(callSuper = true, exclude = {"user"})
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contact extends BaseModel {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @Embedded
  @Valid
  @NotNull
  private ContactInfo info;

  @Pattern(regexp = "\\+\\d{3}\\(\\d{2}\\)\\d{7}")
  @NotEmpty
  private String mobilePhone;

  private String homePhone;

  @Email
  private String email;

  private String address;
}
