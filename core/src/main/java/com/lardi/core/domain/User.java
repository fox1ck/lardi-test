package com.lardi.core.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Table(name = "user")
@Entity
@Data
@ToString(callSuper = true, exclude = {"contacts"})
@EqualsAndHashCode(callSuper = true, exclude = {"contacts"})
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseModel {

  @Column(unique = true)
  private String login;

  private String passwordHash;

  @Embedded
  private ContactInfo info;

  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Contact> contacts;

}
