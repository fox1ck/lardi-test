package com.lardi.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactInfo {

  @NotEmpty
  @Size(min = 5, max = 50)
  private String firstName;

  @NotEmpty
  @Size(min = 5, max = 50)
  private String lastName;

  @NotEmpty
  @Size(min = 5, max = 50)
  private String middleName;
}
