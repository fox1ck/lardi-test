package com.lardi.core.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContactDto {

  private Long id;

  private ContactInfo info;

  private String mobilePhone;

  private String homePhone;

  private String email;

  private String address;

}
