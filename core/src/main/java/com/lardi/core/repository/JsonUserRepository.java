package com.lardi.core.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lardi.core.config.StorageProperties;
import com.lardi.core.domain.Contact;
import com.lardi.core.domain.User;
import com.lardi.core.exception.RuntimeIOException;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

@RequiredArgsConstructor
public class JsonUserRepository implements CustomUserRepository {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final Random RANDOM = new Random();

  private final StorageProperties storageProperties;

  @Override
  public Optional<User> findByLogin(String login) {
    try {
      User user = MAPPER.readValue(createFileByLogin(login), User.class);
      return Optional.ofNullable(user);
    } catch (IOException e) {
      return Optional.empty();
    }
  }

  @Override
  public User save(User user) {
    setRootIndex(user);
    setContactIndexesAndRemoveUserAssociation(user);
    try {
      MAPPER.writeValue(createFileByLogin(user.getLogin()), user);
    } catch (IOException e) {
      throw new RuntimeIOException(e);
    }
    return user;
  }

  private void setContactIndexesAndRemoveUserAssociation(User user) {
    Set<Contact> contacts = user.getContacts();
    if (contacts != null) {
      for (Contact contact : contacts) {
        contact.setUser(null);
        if (contact.getId() == null) {
          contact.setId(generateId());
        }
      }
    }
  }

  private void setRootIndex(User user) {
    if (user.getId() == null) {
      user.setId(generateId());
    }
  }

  private File createFileByLogin(String login) {
    return Paths.get(storageProperties.getDataDir(), login).toFile();
  }

  private Long generateId() {
    return System.nanoTime() + RANDOM.nextInt(100);
  }

}
