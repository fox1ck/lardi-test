package com.lardi.core.repository;

import com.lardi.core.domain.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomUserRepository {
  Optional<User> findByLogin(String login);

  User save(User user);
}
