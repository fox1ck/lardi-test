class Contact {
    constructor(id, contactInfo = new ContactInfo(), mobilePhone, homePhone, email, address) {
        this.id = id;
        this.info = contactInfo;
        this.mobilePhone = mobilePhone;
        this.homePhone = homePhone;
        this.email = email;
        this.address = address;
    }


    format() {
        return `
        <tr id="${contactTag}${this.id}">
            <td>${this.info.firstName}</td>
            <td>${this.info.lastName}</td>
            <td>${this.info.middleName}</td>
            <td>${this.mobilePhone}</td>
            <td>${this.homePhone === undefined ? '' : this.homePhone}</td>
            <td>${this.email === undefined ? '' : this.email}</td>
            <td>${this.address === undefined ? '' : this.address}</td>
        </tr>`;
    }

    static fromJson(json) {
        return new Contact(json.id, ContactInfo.fromJson(json.info), json.mobilePhone, json.homePhone, json.email, json.address);
    }
}

class ContactInfo {
    constructor(firstName, lastName, middleName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }

    static fromJson(json) {
        return new ContactInfo(json.firstName, json.lastName, json.middleName);
    }
}

let contacts = [];
let table = $('#contact-table');
let contactTag = 'contact_';
let selectedContact = new Contact();

function addContact() {
    fillCurrentContactFromFormData();
    $.ajax({
        url: 'contacts',
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(selectedContact)
    }).then((onSuccess) => {
        let fromJson = Contact.fromJson(onSuccess);
        let number = contacts.findIndex(e => e.id === fromJson.id);
        if (number !== -1) {
            contacts[number] = fromJson;
        } else {
            contacts.push(fromJson);
        }
        renderContacts();
        resetToDefaultState();
        showNotify('SUCCESS', 'Added contact', 'success');
    }, ((reason) => {
        showNotify('ERROR', reason.responseText, 'error');
    }));
}

function updateContact() {
    fillCurrentContactFromFormData();
    $.ajax({
        url: `contacts/${selectedContact.id}`,
        method: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(selectedContact)
    }).then((onSuccess) => {
        let fromJson = Contact.fromJson(onSuccess);
        let number = contacts.findIndex(e => e.id === fromJson.id);
        if (number !== -1) {
            contacts[number] = fromJson;
        } else {
            contacts.push(fromJson);
        }
        renderContacts();
        resetToDefaultState();
        showNotify('SUCCESS', 'Updated contact', 'success');
    }, ((reason) => {
        showNotify('ERROR', reason.responseText, 'error');
    }));
}

function deleteContact() {
    $.ajax({
        url: `contacts/${selectedContact.id}`,
        method: 'DELETE'
    }).then((success) => {
            let number = contacts.findIndex(e => e.id === selectedContact.id);
            if (number !== -1) {
                contacts.splice(number, 1);
            }
            renderContacts();
            resetToDefaultState();
            showNotify('SUCCESS', 'Deleted contact', 'success');
        },
        (reject) => {
            console.log(reject);
            showNotify('ERROR', 'Some error message', 'error');
        });
}

function loadAllContacts() {
    return $.ajax({
        url: 'contacts',
    }).done(function (data) {
        $.each(data, function (index, value) {
            contacts.push(Contact.fromJson(value));
        });
    }).fail(function (error) {
        console.log(error);
    });
}


function renderContacts() {
    let tableBody = table.find('tbody');
    tableBody.empty();
    for (let i = 0; i < contacts.length; i++) {
        tableBody.append(contacts[i].format());
    }
    $('#contact-table > tbody > tr').click(function (e) {
        let tempId = e.currentTarget.id;
        selectedContact = findContactById(tempId);
        $('#form-delete-button').prop('disabled', false);
        $('#contact-form').prop('action', 'javascript:updateContact()');
        let submitButton = $('#form-submit-button');
        submitButton.text('UPD');
        submitButton.removeClass('is-success');
        submitButton.addClass('is-info');
        fillForm(selectedContact);
    });
}

function findContactById(tempId) {
    let id = Number(tempId.substring(contactTag.length, tempId.length));
    return contacts.find(e => e.id === id);
}

function fillForm(contact) {
    $('#form-first-name').val(contact.info.firstName);
    $('#form-last-name').val(contact.info.lastName);
    $('#form-middle-name').val(contact.info.middleName);
    $('#form-mobile-phone').val(contact.mobilePhone);
    $('#form-home-phone').val(contact.homePhone);
    $('#form-address').val(contact.address);
    $('#form-email').val(contact.email);
}

function fillCurrentContactFromFormData() {
    if (selectedContact) {
        selectedContact.info.firstName = returnNullIfEmptyString($('#form-first-name').val());
        selectedContact.info.lastName = returnNullIfEmptyString($('#form-last-name').val());
        selectedContact.info.middleName = returnNullIfEmptyString($('#form-middle-name').val());
        selectedContact.mobilePhone = returnNullIfEmptyString($('#form-mobile-phone').val());
        selectedContact.homePhone = returnNullIfEmptyString($('#form-home-phone').val());
        selectedContact.address = returnNullIfEmptyString($('#form-address').val());
        selectedContact.email = returnNullIfEmptyString($('#form-email').val());
    }
}

function returnNullIfEmptyString(src) {
    return src.length === 0 ? null : src;
}

function resetToDefaultState() {
    let contactForm = $('#contact-form');
    let submitButton = $('#form-submit-button');
    selectedContact = new Contact();
    $('#filter-input').val('');
    $('#form-delete-button').prop('disabled', true);
    contactForm.get(0).reset();
    contactForm.prop('action', 'javascript:addContact()');
    submitButton.text('ADD');
    submitButton.removeClass('is-info');
    submitButton.addClass('is-success');
}

function filterTable() {
    let val = $('#filter-input').val();
    if (val.length === 0) {
        table.find('tbody > tr').show();
    } else {
        table.find('tbody > tr:not(:contains(' + val + '))').hide();
    }
}

function showNotify(title, text, type) {
    new PNotify({
        title: title,
        text: text,
        type: type
    });
}