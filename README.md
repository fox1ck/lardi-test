SET FOREIGN_KEY_CHECKS=0;

drop table user;
drop table contact;

create table user
(
	id bigint auto_increment
		primary key,
	login varchar(16) not null,
	password_hash varchar(100) not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	middle_name varchar(50) not null,
	constraint user_id_uindex
		unique (id),
	constraint user_login_uindex
		unique (login)
)
engine=InnoDB
;

create table contact
(
	id bigint auto_increment
		primary key,
	user_id bigint not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	middle_name varchar(50) not null,
	mobile_phone varchar(15) not null,
	home_phone varchar(15) null,
	email varchar(50) null,
	address varchar(50) null,
	constraint contact_id_uindex
		unique (id),
	constraint contact_user_id_mobile_phone_uindex
		unique (user_id, mobile_phone),
	constraint contact_user_id_fk
		foreign key (user_id) references user (id)
)
engine=InnoDB
;

SET FOREIGN_KEY_CHECKS=1;